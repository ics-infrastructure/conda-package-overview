import conda.exports
import json
from operator import itemgetter
import yaml
import requests


def get_tree(channel, platform):
    print("Generating tree data")
    index = conda.exports.get_index((channel,), prepend=False, platform=platform)
    data = {"name": "Packages", "children": []}
    for package in index.values():
        dependency = []
        for depends in package["depends"]:
            dependency.append({"name": depends})
        if search(data["children"], package["name"]):
            index_nr = find(data["children"], key="name", value=package["name"])
            data["children"][index_nr]["children"].append(
                {
                    "name": package["version"] + " " + package["build"],
                    "children": dependency,
                }
            )
        else:
            data["children"].append(
                {
                    "name": package["name"],
                    "children": [
                        {
                            "name": package["version"] + " " + package["build"],
                            "children": dependency,
                        }
                    ],
                }
            )

    with open(
        "./conda_packages/static/json/conda_package_info_tree.json", "w"
    ) as outfile:
        json.dump(data, outfile)


def get_graph(channel, platform):
    print("Generating graph data")
    index = conda.exports.get_index((channel,), prepend=False, platform=platform)
    data = {"label": "Packages", "id": "Packages", "children": []}
    package_index = -1
    for package in index.values():
        dependency = []
        for depends in package["depends"]:
            dependency.append(
                {
                    "id": depends,
                    "label": depends,
                    "isDisabled": "true",
                    "ancestor": package["name"] + " pkg",
                    "parent": package["version"] + " " + package["build"],
                }
            )
        if not package["name"] + " pkg" in map(itemgetter("id"), data["children"]):
            package_index += 1
            data["children"].append(
                {
                    "id": package["name"] + " pkg",
                    "label": package["name"],
                    "children": [
                        {
                            "parent": package["name"] + " pkg",
                            "id": package["version"],
                            "label": package["version"],
                            "children": [
                                {
                                    "grandparent": package["name"] + " pkg",
                                    "parent": package["version"],
                                    "id": package["version"] + " " + package["build"],
                                    "label": package["build"],
                                    "children": dependency,
                                }
                            ],
                        }
                    ],
                }
            )
        # check if package name exist
        else:
            for child_place, child in enumerate(data["children"]):
                if package["name"] + " pkg" == child["id"] and not package[
                    "version"
                ] in map(itemgetter("id"), child["children"]):
                    data["children"][child_place]["children"].append(
                        {
                            "parent": package["name"] + " pkg",
                            "id": package["version"],
                            "label": package["version"],
                            "children": [],
                        }
                    )

                for grandchild_place, grandchild in enumerate(child["children"]):
                    if package["version"] == grandchild["id"]:
                        data["children"][child_place]["children"][grandchild_place][
                            "children"
                        ].append(
                            {
                                "grandparent": package["name"] + " pkg",
                                "parent": package["version"],
                                "id": package["version"] + " " + package["build"],
                                "label": package["build"],
                                "children": dependency,
                            }
                        )

    with open(
        "./conda_packages/static/json/conda_package_info_graph.json", "w"
    ) as outfile:
        json.dump(data, outfile)


def get_table(channel, platform):
    print("Generating table data")
    index = conda.exports.get_index((channel,), prepend=False, platform=platform)
    data = []
    for package in index.values():
        dependency = []
        data.append(
            {
                "name": package.name,
                "version": package.version,
                "build": package.build,
                "dependency": dependency,
            }
        )
        for depends in package.depends:
            dependency.append({"name": depends})

    with open(
        "./conda_packages/static/json/conda_package_info_table.json", "w"
    ) as outfile:
        json.dump(data, outfile)


def get_pinned(channel, platform):
    print("Generating pinned packages data")
    r = requests.get(
        "https://gitlab.esss.lu.se/e3-recipes/e3-pinning/raw/master/conda_build_config.yaml"
    )
    pinned_packages = yaml.safe_load(r.content)
    index = conda.exports.get_index((channel,), prepend=False, platform=platform)
    data = {"name": "Packages", "children": []}
    result = {}
    for package in index.values():
        if package['name'] in pinned_packages and package['version'] == pinned_packages[package.name][0]:
            if package.name not in result:
                result[package.name] = package
            else:
                if package.timestamp > result[package.name].timestamp:
                    result[package.name] = package

    # for pinned in result:
    #     dependency = []
    #     for depends in result[pinned]["depends"]:
    #         dependency.append({"name": depends})
    #     if search(data["children"], result[pinned]["name"]):
    #         index_nr = find(data["children"], key="name", value=result[pinned]["name"])
    #         data["children"][index_nr]["children"].append(
    #             {
    #                 "name": result[pinned]["version"] + " " + result[pinned]["build"],
    #                 "children": dependency,
    #             }
    #         )
    #     else:
    #         data["children"].append(
    #             {
    #                 "name": result[pinned]["name"],
    #                 "children": [
    #                     {
    #                         "name": result[pinned]["version"] + " " + result[pinned]["build"],
    #                         "children": dependency,
    #                     }
    #                 ],
    #             }
    #         )



    for pinned in result:
        dependency = []
        for depends in result[pinned]["depends"]:
            dependency.append({"id": depends, "parent": result[pinned]["name"] + " " + result[pinned]["version"] + " " + result[pinned]["build"], "ancestor": "true","isDisabled": "true",})
        data["children"].append(
            {
                "id": result[pinned]["name"] + " " + result[pinned]["version"] + " " + result[pinned]["build"],
                "children": dependency
            }
        )

    with open(
        "./conda_packages/static/json/conda_package_info_pinned.json", "w"
    ) as outfile:
        json.dump(data, outfile)

def return_index():
    return True


def find(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1


def search(values, searchFor):
    for k in values:
        for v in k:
            if searchFor in k[v]:
                return True
    return False


if __name__ == "__main__":
    get_tree("conda-e3", "linux-64")
    get_graph("conda-e3", "linux-64")
    get_table("conda-e3", "linux-64")
    get_pinned("conda-e3", "linux-64")
