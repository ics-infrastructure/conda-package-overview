import datetime
import json
from pathlib import Path
from flask import Flask, g, render_template, jsonify


app = Flask(__name__)
app.config.from_mapping(
    FREEZER_DESTINATION="../public",
    FREEZER_BASE_URL="http://ics-infrastructure.pages.esss.lu.se/conda-package-overview",
    FREEZER_DEFAULT_MIMETYPE="application/json",
    CONDA_PACKAGES_INFO=Path(app.root_path)
    / "static/json/conda_package_info_table.json",
)


def conda_packages_info():
    info = getattr(g, "_conda_packages_info_table", None)
    if info is None:
        with open(app.config["CONDA_PACKAGES_INFO"]) as f:
            info = g._conda_packages_info = json.load(f)
    return info


@app.context_processor
def current_time():
    return dict(current_time=datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M"))


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/conda_tree/")
def list_e3_modules():
    return render_template("conda_tree.html")


@app.route("/conda_packages/")
def list_conda_packages():
    info = conda_packages_info()
    return render_template("conda_table.html", packages=info)

@app.route("/conda_dependency/")
def list_dependencies():
    return render_template("conda_dependency.html")
