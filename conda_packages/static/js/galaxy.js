$(document).ready(function () {

  $('#conda_packages_table').DataTable(
    {
      "paging": false,
      "info": false,
      "order": [[0, 'asc'], [1, 'asc']],
      "columnDefs": [
        {
          "visible": false,
          "targets": [0, 1]
        },
        {
          "targets": [0, 1, 2, 3],
          "orderable": false
        }
      ],
      "rowGroup": {
        dataSrc: [
          0,
          1
        ]
      }

    }
  );

});
