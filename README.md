# ansible-galaxy

This repository contains all ICS Ansible roles and playbooks from the
[ics-ansible-galaxy](https://gitlab.esss.lu.se/ics-ansible-galaxy) group.

Also included are all templates from AWX.

It uses Jinja2, Flask and Frozen-Flask for site creation.

The E3 modules display page uses Select2 for its search box and D3 for the collapsible tree (forked from https://gist.github.com/jjzieve/a743242f46321491a950)
it uses the conda API for its data

It is automatically updated by a GitLab bot: [galaxy-bot](https://gitlab.esss.lu.se/ics-infrastructure/galaxy-bot).

The dependency graph is modified from
https://bl.ocks.org/agnjunio/fd86583e176ecd94d37f3d2de3a56814
and uses D3 and vue.js
License
-------

MIT
