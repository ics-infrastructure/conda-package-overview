from flask_frozen import Freezer
from conda_packages import app

freezer = Freezer(app)


if __name__ == "__main__":
    freezer.freeze()
